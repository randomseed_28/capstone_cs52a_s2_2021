#!/usr/bin/env python
# coding: utf-8

from SignDetector import SignDetector

class Trafficsignscv:
    def __init__(self):
        self.detected_sign = []
        self.sign_location = []
        self.detected_size = []
        self.score_threshold = 0.70

        self.possible_signs = ['black_left_round', 'black_right_round', 'black_white_left', 'black_white_right',
        'blue_left_arrow', 'blue_right_arrow', 'blue_left_round', 'orange_cone', 'park_green',
        'park_white', 'park_yellow', 'blue_right_round', 'stop', 'traffic_lights_A', "traffic_lights_G",
        'traffic_lights_R', 'speed_5', 'speed_10', 'speed_25', 'speed_40', 'speed_50']

        self.SignDetector = SignDetector()

    # return detected labels
    def run(self, img_arr):
        if img_arr is None:
            self.detected_sign = []
            self.detected_size = []
            return self.detected_sign, self.sign_location,self.detected_size

        self.detected_sign = []
        self.sign_location = []
        self.detected_size = []


        bboxes_, scores_, class_max_index_ = self.SignDetector.get_labels(img_arr)

        for index in range(len(class_max_index_)):
            if scores_[index] < self.score_threshold:
                continue
            self.detected_sign.append(self.possible_signs[class_max_index_[index]])
            w = int(bboxes_[index][2])-int(bboxes_[index][0])
            h = int(bboxes_[index][3])-int(bboxes_[index][1])
            loc_x = int((bboxes_[index][2]+int(bboxes_[index][0]))/2)
            loc_y = int((bboxes_[index][3]+int(bboxes_[index][1]))/2)
            size = w*h
            location = [loc_x, loc_y]
            self.detected_size.append(size)
            self.sign_location.append(location)

        print("+++++++++ model outputs +++++++++")
        print(f"Signs: {self.detected_sign}")
        print(f"Location: {self.sign_location}")
        print(f"Size: {self.detected_size}")
        print()


        return self.detected_sign, self.sign_location, self.detected_size

    def update(self):
        pass

    def run_threaded(self, img_arr):
        return self.run(img_arr)

