#!/usr/bin/env python3
"""
Scripts for operating the OpenCV by Robotics Masters
 with the Donkeycar
author: @wallarug (Cian Byrne) 2020
modified by: USYD capstone team CS52A, 2021 (Jun Liu, Shaoyue Guo)
contrib: @peterpanstechland 2020

Note: To be used with code.py bundled in this repo. See donkeycar/contrib/robohat/code.py
"""

import time
import donkeycar as dk
import cv2

from action_response.rmracerlibrary import Trafficsignscv

# TODO:  Import the working OpenCV libraries here
# from donkeycar.parts.rmracerlib.trafficsignscv import detect

traffic_cv = Trafficsignscv()

COUNTER_THRESHOLD = 5  # will take (+1) of value
COUNTER_SIZE = 10  # 1 = ~0.05 secs, 20 = 1 sec

ACTION_BUFFER = 1  # 10 = 0.5s, 20 = 1 sec
TURN_ACTION_TIME = 39  # 60 = 3.0s, 20 = 1 sec
STOP_ACTION_TIME = 30
i = 200
LOAD_TIME_WAIT_SEC = 5

NO_REPEAT_ACTION_TIME = 30


class RMRacerCV:
    '''
    This class implements the action response logic upon detecting a specific traffic sign.
    All signs are categorised into stop, park, left/right turn, traffic cone and traffic lights.
    Each sign categorisation has a unique response logic.

    '''

    def __init__(self, cfg, debug=False):
        self.throttle = 0
        self.steering = 0
        self.debug = debug

        # actions for the car & error detection
        self.stop_sign = Queue10(COUNTER_SIZE)
        self.right_sign = Queue10(COUNTER_SIZE)
        self.left_sign = Queue10(COUNTER_SIZE)
        self.park_sign = Queue10(COUNTER_SIZE)
        self.cone_sign = Queue10(COUNTER_SIZE)

        # if we are already running a sequence (like parking) do
        #  not do anything except finish that sequence
        self.running = False
        self.action = None
        self.wait = 0



        # capture the speed of the car just before carrying out relevant actions
        self.stop_speed = None
        self.bypass_speed = None
        self.park_speed = None

        # The brake value which determines how quickly the car will be stopped
        self.brake_strength_index = 0.15

        # the flag to mark the stop action before turning/stopping
        # to ensure that the stop action in turning only executes once
        self.turning_stop = True
        self.parking_stop = True

        # This previous action and the cooldown value are to prevent the car from
        # reacting to the same sign repeatedly
        self.prev_action = None
        self.cooldown = 0

        # this value is to be read from the speed limit sign
        self.speed_limit = None


    def run(self, img_arr, throttle, steering, speed, debug=False):
        if throttle is None or steering is None:
            return 0, 0, img_arr


        # use in stop action, when speed below this threshold set throttle to 0
        speed_thres = 0.1



        # terminal session
        print("++++++++++ loop ++++++++++")
        print(f"previous move: {self.prev_action}")
        print(f"action cooldown: {self.cooldown}")
        print(f"current action: {self.action}")
        print(f"delay:{self.wait}")
        print(f"speed limit: {self.speed_limit}")
        print(f"actioning? {self.running}")
        # print(f"brake:{brake}")
        print("--- car status ---")
        print(f"throttle: {self.throttle}")
        print(f"steering: {self.steering}")
        print(f"speed:{speed}")
        print()





        self.throttle = throttle
        self.steering = steering

        # read inputs from the sign detection model
        signs, locations, pixels = traffic_cv.run(img_arr)

        # If the action cooldown counts are reached, reset the cooldown value and the flag
        if self.cooldown > NO_REPEAT_ACTION_TIME:
            self.prev_action = None
            self.cooldown = 0
        if self.prev_action:
            self.cooldown += 1

        if (img_arr is not None) and len(signs) > 0:
            sign = signs[0]
            location = locations[0]
            pixel = pixels[0]
            print("sign", sign, "pixel", pixel)

            # print in the terminal the speed limit value shown on the sign
            if sign == "speed_5" or sign == "speed_10" or sign == "speed_25" or sign == "speed_40" or sign == "speed_50":
                split_str = sign.split('_')
                speed_limit = int(split_str[1])
                self.speed_limit = speed_limit

            else:
                self.speed_limit = None

        # when no signs are detected:
        if self.running == False:
            if (img_arr is None) or len(signs) == 0:
                self.throttle = throttle
                self.steering = steering
                return throttle, steering, img_arr

            sign = signs[0]
            location = locations[0]
            pixel = pixels[0]



                # -------------------- stop sign detection --------------------------
            if sign == "stop" and pixel > 750:
                if throttle <= 0:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr

                if self.prev_action == "stop" and self.cooldown <= NO_REPEAT_ACTION_TIME:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr

                self.running = True
                print("++++++++ Start stopping ++++++++++")
                self.action = "stopping"
                self.stop_speed = speed
                self.throttle = throttle
                self.steering = steering
                return throttle, steering, img_arr

            else:
                self.stop_sign.put()

            # -------------------- traffic lights detection --------------------------
            if sign == "traffic_lights_R" and pixel > 400:
                if throttle <= 0:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr

                self.running = True
                print(" +++++ red light detected +++++  ")
                self.action = "light_stopping"
                self.stop_speed = speed
                self.throttle = throttle
                self.steering = steering
                return throttle, steering, img_arr

            else:
                self.stop_sign.put()

            # -------------------- right turn sign detection --------------------------
            if (sign == "black_right_round" or sign == "black_white_right" or sign == "blue_right_round" or sign == "blue_right_arrow") and pixel > 900:
                if throttle <= 0:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr

                print("-------right sign detected------ ")
                self.action = "right"
                self.running = True
                return throttle, steering, img_arr

            # -------------------- left turn sign detection --------------------------
            elif (sign == "black_left_round" or sign == "black_white_left" or sign == "blue_left_round" or sign == "blue_left_arrow") and pixel > 900:
                if throttle <= 0:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr

                print("-------left sign detected------ ")
                if self.left_sign.put(1) > COUNTER_THRESHOLD:
                    self.running = True
                    self.action = "left"
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr


            else:
                self.right_sign.put()
                self.left_sign.put()

            # -------------------- park sign detection --------------------------
            if (sign == "park_green" or sign == "park_white" or sign == "park_yellow") and pixel > 900:
                if self.prev_action == "park" and self.cooldown <= NO_REPEAT_ACTION_TIME:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr

                if self.park_sign.put(1) >= 3:
                    self.running = True
                    self.action = "park"
                    self.throttle = throttle
                    self.steering = steering
                    self.park_speed = speed
                    return throttle, steering, img_arr
            else:
                self.park_sign.put()

            # -------------------- traffic cone detection --------------------------
            if sign == "orange_cone" and pixel > 450 and location[0] >= 104 and location[0] <= 312:
                if throttle <= 0:
                    self.throttle = throttle
                    self.steering = steering
                    return throttle, steering, img_arr


                if self.cone_sign.put(1) > 3:
                    self.running = True
                    self.action = "cone"

                    self.throttle = throttle
                    self.steering = steering
                    return self.throttle, self.steering, img_arr

            else:
                self.cone_sign.put()




        # actions are carried out once detects a sign
        if self.running == True:

            if self.stop_speed:
                # use in stopping, when speed below this threshold set throttle to 0
                # speed_thres : larger value stands for shorter reaction time
                speed_thres = self.stop_speed * 0.25
                if speed_thres > 0.25:
                    speed_thres = 0.25




            # ---------------------- action for stopping ----------------------------
            # this will be the same for stop signs, red light and stopping when making a turn
            if (self.action == "stopping" or self.action == "light_stopping") and speed > speed_thres:  # wait before stopping
                # self.wait += 1
                print("+++++++++ stopping in progress +++++++")

                # set throttle value to negative to speed up the stopping process
                # brake_strength: larger value stands for stronger brake power
                brake_strength = -self.stop_speed * self.brake_strength_index

                self.throttle = brake_strength
                self.steering = steering

                return brake_strength, steering, img_arr


            elif (self.action == "stopping" or self.action == "light_stopping") and speed <= speed_thres:  # execute
                if self.action == "stopping":
                    self.action = "stopped"

                elif self.action == "light_stopping":
                    self.action = "stopped_at_light"
                self.stop_speed = None

                self.throttle = 0
                self.steering = 0

                return self.throttle, self.steering, img_arr


            elif self.action == "stopped" and self.wait <= STOP_ACTION_TIME:  # execute, action is running
                self.wait += 1

                self.throttle = 0
                self.steering = 0
                return self.throttle, self.steering, img_arr

            elif self.action == "stopped" and self.wait > STOP_ACTION_TIME:  # complete, leave action
                self.wait = 0
                self.action = None

                self.prev_action = "stop"
                self.running = False
                self.stop_sign.clear()

                self.throttle = throttle
                self.steering = steering

                return throttle, steering, img_arr

            elif self.action == "stopped_at_light":
                current_light, box_loc, box_size = traffic_cv.run(img_arr)

                # if the light is still red, keep waiting
                if current_light:
                    if current_light[0] == "traffic_lights_R":
                        self.throttle = 0
                        self.steering = 0
                        return self.throttle, self.steering, img_arr


                # Otherwise go
                else:
                    self.action = None

                    self.running = False

                    self.throttle = throttle
                    self.steering = steering
                return throttle, steering, img_arr



            # -------------------------- action for turning -----------------------------
            # stop the car upon detecting a turning sign, then make the turning action
            # This is to reduce the complexity of tuning parameters
            elif (self.action == "left" or self.action == "right") and self.wait < ACTION_BUFFER:
                print(f"+++++++++ ready to turn +++++++")
                self.wait += 1
                self.stop_speed = speed
                self.throttle = throttle
                self.steering = steering

                return throttle, steering, img_arr

            elif (self.action == "left" or self.action == "right") and self.wait >= ACTION_BUFFER:
                # stop the car first
                if speed > speed_thres and self.turning_stop:
                    brake_strength = -self.stop_speed * self.brake_strength_index

                    self.throttle = brake_strength
                    self.steering = steering


                    return self.throttle, self.steering, img_arr

                else:
                    self.turning_stop = False
                    if self.action == "left" and self.wait < ACTION_BUFFER + TURN_ACTION_TIME:  # (ACTION_BUFFER + ACTION_TIME)
                        print(f"+++++++++ {self.action} turning in progress +++++++")
                        self.wait += 1
                        self.throttle = 0.7
                        self.steering = -0.7
                        return self.throttle, self.steering, img_arr
                    elif self.action == "right" and self.wait < ACTION_BUFFER + TURN_ACTION_TIME:  # (ACTION_BUFFER + ACTION_TIME)
                        print(f"+++++++++ {self.action} turning in progress +++++++")
                        self.wait += 1
                        self.throttle = 0.7
                        self.steering = 0.7
                        return self.throttle, self.steering, img_arr
                    else:
                        self.wait = 0
                        self.action = None
                        self.stop_speed = None
                        self.steering = 0
                        self.running = False
                        self.turning_stop = True
                        self.left_sign.clear()
                        self.right_sign.clear()

                        self.throttle = throttle
                        self.steering = steering
                        return self.throttle, self.steering, img_arr

            # -------------------------- action for parking -----------------------------


            elif self.action == "park" and self.wait < 7:
                self.wait += 1
                self.throttle = 0
                self.steering = steering
                self.stop_speed = speed
                return self.throttle, self.steering, img_arr



            elif self.action == "park" and self.wait >= 7:
                park_time = 30

                print(f"park time:  {park_time}")

                # stop the car before parking
                if speed > speed_thres and self.parking_stop:
                    brake_strength = -self.stop_speed * self.brake_strength_index

                    self.throttle = brake_strength
                    self.steering = steering


                    return self.throttle, self.steering, img_arr

                else:
                    self.parking_stop = False





                # First steer the car into the parking slot,
                # then adjust its direction back to front.
                # After waiting for some period, steer the car out of the parking slot.
                if self.wait <= park_time:

                    self.wait += 1
                    print("steering to the parking slot")

                    self.throttle = 0.5
                    self.steering = -0.7
                    return self.throttle, self.steering, img_arr

                elif self.wait <= 1.3*park_time:
                    print("steering back to position")
                    self.wait += 1
                    self.throttle = 0.5
                    self.steering = 0.9
                    self.stop_speed = speed
                    return self.throttle, self.steering, img_arr

                elif self.wait > 40 and self.wait <= 70:
                    if speed > speed_thres:
                        brake_strength = -self.stop_speed * self.brake_strength_index

                        self.throttle = brake_strength
                        self.steering = steering

                        return self.throttle, self.steering, img_arr

                    else:
                        self.wait += 1
                        self.throttle = 0
                        self.steering = 0
                        return self.throttle, self.steering, img_arr  # stop the car

                elif self.wait <= 90:
                    self.wait += 1
                    self.throttle = 0.8
                    self.steering = 0.7
                    return self.throttle, self.steering, img_arr  # stop the car

                elif self.wait > 90:
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.park_sign.clear()
                    self.stop_speed = None
                    self.prev_action = "park"
                    self.park_speed = None
                    self.parking_stop = True

                    self.throttle = throttle
                    self.steering = steering
                    return self.throttle, self.steering, img_arr

            # -------------------------- action for bypassing cones -----------------------------
            elif self.action == "cone" and self.wait < 3:
                self.wait += 1
                self.throttle = throttle
                self.steering = steering
                return throttle, steering, img_arr

            elif self.action == "cone" and self.wait >= 3:
                self.wait = 0
                self.action = "bypassing"
                # capture the speed when initiating the bypass action
                self.bypass_speed = speed
                self.throttle = 0
                self.steering = steering
                return self.throttle, self.steering, img_arr


            elif self.action == "bypassing":
                # A simple function to dynamically adjust the angle and duration of the bypassing action
                # according to the speed of the car
                bypass_angle = 0.6 / self.bypass_speed
                bypass_time = 12 / self.bypass_speed

                if bypass_time > 15:
                    bypass_time = 15

                if bypass_angle > 0.8:
                    bypass_angle = 0.8

                # set a minimum angle
                # if bypass_angle < 0.25:
                #     bypass_angle = 0.25

                print(f"bypassing time: {bypass_time}")
                if self.wait <= bypass_time:
                    self.wait += 1
                    self.throttle = 0.1
                    self.steering = bypass_angle
                    return self.throttle, self.steering, img_arr

                elif self.wait <= 1.8*bypass_time:
                    self.wait += 1
                    self.throttle = 0.1
                    self.steering = -1.1*bypass_angle
                    return self.throttle, self.steering, img_arr

                elif self.wait <= 2*bypass_time:
                    self.wait += 1
                    self.throttle = 0.1
                    self.steering = 0
                    return self.throttle, self.steering, img_arr

                else:
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.bypass_speed = None
                    self.cone_sign.clear()
                    self.throttle = throttle
                    self.steering = steering
                    return self.throttle, self.steering, img_arr

        ##
        ## Action Check Status
        ##

    def update(self):
        pass

    def run_threaded(self, img_arr, throttle, steering, speed):
        return self.run(img_arr, throttle, steering, speed)


class Queue10:
    '''
    A Stack that only holds 10 items, nothing more, nothing less
    '''

    def __init__(self, size):
        self._list = [0] * size
        self._index = 0
        self.size = size
        self.total = 0

    def put(self, value=0):
        self._list[self._index] = value
        self._increment()
        return self.total

    def _increment(self):
        if self._index == self.size - 1:
            self._index = 0
        else:
            self._index += 1
        self.total = sum(self._list)

    def clear(self):
        while (i < self.size):
            self.put(0)

    def sneaky(self):
        return self._list