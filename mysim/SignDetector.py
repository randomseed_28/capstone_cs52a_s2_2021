from models.experimental import *
from utils.datasets import *
from utils.utils import *

# import tensorflow.compat.v1 as tf

#
# tf.disable_v2_behavior()
# import tensorflow as tf
import random
import colorsys
import cv2
import numpy as np
import os


class SignDetector:
    def __init__(self):
        device = 'cpu'
        device = torch_utils.select_device(device)
        self.device = device
        weights = './yolov5s_se.pt'
        model = attempt_load(weights, map_location=device)
        self.model = model
        self.imgsz = 416
        self.view_img = True

        # Get names and colors
        names = model.module.names if hasattr(model, 'module') else model.names
        colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]
        self.names = names
        self.colors = colors

        self.conf_thres = 0.5
        self.iou_thres = 0.5

    def run(self, origin_image):
        # Padded resize
        img = letterbox(origin_image, new_shape=self.imgsz)[0]

        # Convert
        #img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = img.transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)
        image_height, image_width, channel = origin_image.shape
        img = torch.from_numpy(img).to(self.device)
        img = img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        pred = self.model(img, False)[0]
        pred = non_max_suppression(pred, self.conf_thres, self.iou_thres)
        result = []
        bboxes_ = []
        scores_ = []
        class_max_index_ = []
        for i, det in enumerate(pred):
            gn = torch.tensor(origin_image.shape)[[1, 0, 1, 0]]
            if det is not None and len(det):
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], origin_image.shape).round()
                for *xyxy, conf, cls in det:
                    xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                    label = '%s %.2f' % (self.names[int(cls)], conf)
                    plot_one_box(xyxy, origin_image, label=label, color=self.colors[int(cls)], line_thickness=1)
        if det is not None:
            det = det.cpu().detach().numpy()
            for one_result in det:
                bboxes_.append(one_result[:4].astype(np.int32).tolist())
                scores_.append(float(one_result[4]))
                class_max_index_.append(int(one_result[5]))
                result.append([one_result[i] for i in range(len(one_result))])
        if self.view_img:
            img_detection = cv2.cvtColor(origin_image, cv2.COLOR_RGB2BGR)
            cv2.imshow('car', img_detection)
            cv2.waitKey(1)
        return bboxes_, scores_, class_max_index_

    def get_labels(self, img_arr):
        return self.run(img_arr)


    def update(self):
        pass

    def run_threaded(self, img_arr):
        return self.run(img_arr)



# if __name__ == "__main__":
#     # os.environ["CUDA_VISIBLE_DEVICES"] = '0'
#
#     SignDetector = SignDetector()
#
#     image_path = '/Users/pc/Downloads/testforcolab/VOC_test/images/train/'
#     image_list = os.listdir(image_path)
#     for image_name in image_list:
#         image = cv2.imread(os.path.join(image_path, image_name))
#         image_det = SignDetector.run(image)
