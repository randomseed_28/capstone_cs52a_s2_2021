'''
# Sign detection
'''

import cv2
import numpy as np
import platform

import time
import sys

from src import config as cfg
from src.cv.func import sign_direction, direction_check, valid_range, create_trackbars, contours_detection, is_squarish, pentagon_sign_direction, valid_range_bottom

## FILTERS
"""
This section of the code contains all the filters and masks when using colour masking as a technique for sign detection
"""
# red filter for stop sign
lower_red_stop = np.array([160, 75, 50])  
upper_red_stop = np.array([180, 255, 255])   
lower_red2_stop = np.array([0, 150, 100])  
upper_red2_stop = np.array([10, 255, 255])   

# green filter for park sign
lower_green_park = np.array([50, 55, 55])  
upper_green_park = np.array([90, 255, 230])   

# white filter for park sign
lower_white_park = np.array([91, 10, 126])
upper_white_park = np.array([133, 67, 161])

#yellow filter for park sign
lower_yellow_park = np.array([20, 80, 80]) 
upper_yellow_park = np.array([32, 255, 230])

# blue filter for turn arrow
lower_blue_sign = np.array([90, 80, 50])    
upper_blue_sign = np.array([140, 255, 255])   

#filter for turn arrow
pent_turn_lower  = np.array([20, 0, 121]) 
pent_turn_upper = np.array([179, 55, 153])

# orange filter for cone
lower_orange_cone = np.array([0,60,60])
upper_orange_cone = np.array([10,180,120])
lower_orange2_cone = np.array([170,60,60])
upper_orange2_cone = np.array([180,180,120])

#ranges for canny edge detection
canny_low = 50#100
canny_upper = 150#200

# set kernel for operations
kernel = np.ones((cfg.KERNEL_SIZE,cfg.KERNEL_SIZE), np.uint8)
stop_kernel = np.ones((cfg.STOP_KERNEL_SIZE,cfg.STOP_KERNEL_SIZE), np.uint8)

## Sign Detection
"""
This portion of the code contains the techniques used; separated based on the variation of sign
"""

"""
##STOP DETECTION
"""

def detect_stop(frame, hsv):
    mask_red = cv2.inRange(hsv, lower_red_stop, upper_red_stop)
    mask_red2 = cv2.inRange(hsv, lower_red2_stop, upper_red2_stop)
    mask = mask_red + mask_red2
    mask = cv2.GaussianBlur(mask, (3,3), 0)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel, iterations=2)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel, iterations=1)

    img = cv2.bitwise_and(frame,frame,mask = mask)

    # find shapes and contour detection
    height, width = mask.shape[:2]
    cfg.ROOF = int(height*cfg.ROOF_R)
    cfg.HORIZON = int(height*cfg.HORIZON_R)
    contours, _ = cv2.findContours(mask[:cfg.HORIZON, :width], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # find the largest contour first that fits all our conditions.
    largest_area = -1
    rect = None
    largest_cnt = None
    for cnt in contours:

        area = cv2.contourArea(cnt)
        x,y,w,h = cv2.boundingRect(cnt)

        vr = valid_range(x,y,w,h,frame)
        if not vr:
            continue
        if area > largest_area and area > cfg.AREA_SIZE_STOP:
            largest_area = area
            largest_cnt = cnt
            rect = cv2.boundingRect(cnt)
    # we found a rectangle that fits the conditions

    if largest_area > 0:
        # capture a ROI image and store for later
        x,y,w,h = rect
        roi = frame[y:y+h, x:x+w]
        if cfg.DEMO_MODE:
            cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,255), 2)
            cv2.putText(frame, "Stop", (x,y), cfg.FONT, 1, (0,0,255))
        return frame, True
    return frame, False

"""
##TURN DETECTION
"""

def detect_turn(frame, hsv):

#Due to the different turn signs (arrow and circle) we implement a modular approach and have two seaparate detection methods which implement different techniques

    if(detect_penta_turn(frame, hsv)[1] is not None):
        return detect_penta_turn(frame, hsv)
    elif(detect_circle(frame, hsv)[1] is not None):
        return detect_circle(frame, hsv)

    mask = cv2.inRange(hsv, lower_blue_sign, upper_blue_sign)
    mask = cv2.GaussianBlur(mask, (5,5), 0)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=1)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=1)
    img_mask = cv2.bitwise_and(frame,frame,mask = mask)

    # drawing boundaries for debugging
    height, width = mask.shape[:2]
    cfg.HORIZON = int(height*cfg.HORIZON_R)
    cfg.ROOF = int(height*cfg.ROOF_R)

    contours, _ = cv2.findContours(mask[0:cfg.HORIZON, 0:width], cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    
    # find the largest contour first that fits all our conditions.
    largest_area = -1
    rect = None

    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)
        vr = valid_range(x,y,w,h,frame)
        if vr:
            area = cv2.contourArea(cnt)
            if area > largest_area and area > cfg.AREA_SIZE_TURN:
                largest_area = area
                rect = cv2.boundingRect(cnt)

     # if we found an region of interest that fits the conditions
    result = None
    if largest_area > 0:
        x,y,w,h = rect
        roi = frame[y:y+h, x:x+w]
        result = sign_direction(roi)
        if result is not None:
            if cfg.DEMO_MODE:
                cv2.rectangle(frame, (x-3,y-3), (x+w+3, y+h+3), (255,127,0), 1)
                cv2.putText(frame, str(result), (x+w, y+h), cfg.FONT, 1, (255,127,0))
    return frame, result

def detect_circle(frame, hsv):

    result = False

    lower_black_sign = np.array([89, 19, 0])
    upper_black_sign = np.array([120, 39, 66])

    _, _, vframe = cv2.split(hsv)

    # Set our filtering parameters
    # Initialize parameter settiing using cv2.SimpleBlobDetector
    params = cv2.SimpleBlobDetector_Params()
    # Set Area filtering parameters
    params.filterByArea = False
    params.minArea = 10
    # Set Circularity filtering parameters
    params.filterByCircularity = True
    params.minCircularity = 0.7
    # Set Convexity filtering parameters
    params.filterByConvexity = False
    params.minConvexity = 0.2
    # Set inertia filtering parameters
    params.filterByInertia = False
    params.minInertiaRatio = 0.01

    # Create a detector with the parameters
    detector = cv2.SimpleBlobDetector_create(params)

    # Detect blobs
    keypoints = detector.detect(frame)
    i = 0
    p = 0
    o = 0
    colour = False

    while o < len(keypoints):
        if keypoints[o].size < 30:
           keypoints.remove(keypoints[o])
           continue
        o += 1

    # Find the HSV value of the middle of the blobs
    while i < len(keypoints):
        x = keypoints[i].pt[0]
        y = keypoints[i].pt[1]
        pixel = np.array(hsv[int(y), int(x)])

        while p < len(pixel):
            if pixel[p] < upper_black_sign[p] and pixel[p] > lower_black_sign[p]:
                colour = True
                p += 1

            if pixel[p] < upper_blue_sign[p] and pixel[p] > lower_blue_sign[p]:
                colour = True
                p += 1

            else:
                colour = False
                break
        if colour == True:

            blank = np.zeros((1, 1))
            blobs = cv2.drawKeypoints(frame, keypoints, blank, (0, 0, 255),cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            white_found = False
            x_left = x
            x_right = x
            while white_found == False:
                x_left = x_left + 1
                x_right = x_right - 1

                pixel_l = vframe[int(y), int(x_left)]
                pixel_r = vframe[int(y), int(x_right)]
                if pixel_l > pixel_r:
                    if pixel_l > 50:
                        #right turn detected
                        cv2.putText(blobs, "Right", (int(x)+40, int(y)+10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255))
                        white_found = True
                        result = "right"
                    else:
                        continue
                elif pixel_l < pixel_r:
                    if pixel_r > 50:
                        #left turn detected
                        cv2.putText(blobs, "Left", (int(x)+40, int(y)+10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255))
                        white_found = True
                        result = "left"
                    else:
                        continue
                else:
                    continue
        elif colour == False:
            break
        i += 1

    return frame, result

def detect_penta_turn(frame, hsv):

    mask = cv2.inRange(hsv, pent_turn_lower, pent_turn_upper)
    img = cv2.bitwise_and(frame, frame, mask = mask)
    edges = cv2.Canny(img,canny_low,canny_upper)
    mask = cv2.dilate(edges, kernel, iterations=1)
    mask = cv2.erode(mask, kernel, iterations=1)

    height, width = mask.shape[:2]
    cfg.ROOF = int(height*cfg.ROOF_R)
    cfg.HORIZON = int(height*cfg.HORIZON_R)

    contours, _ = cv2.findContours(mask[0:cfg.HORIZON, 0:width], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    largest_area = -1
    rect = None
    largest_cnt = None
    for cnt in contours:
        area = cv2.contourArea(cnt)
        x,y,w,h = cv2.boundingRect(cnt)

        """
        When driving, road signs will either appear normal width or shortened width. They should
        For turn signs, this means that the width should be approximately similar to the height or less.
        This is to avoid capturing random marks on the walls.
        """

        vr = valid_range(x,y,w,h, frame)

        if not vr:
            continue

        if area > largest_area and area > cfg.AREA_SIZE_TURN_PENT:
            largest_area = area
            rect = cv2.boundingRect(cnt)
            largest_cnt = cnt

        # we found a rect that fits the conditions
    result = None
    if largest_area > 0:
        # capture a ROI image and store for later
        x,y,w,h = rect
        roi = frame[y:y+h, x:x+w]
        cv2.rectangle(frame, (x,y), (x+w, y+h), (255,127,0), 2)
        cv2.drawContours(frame, largest_cnt,-1,(0,0,255,1))
        if vr:
            result = pentagon_sign_direction(roi)
    return frame, result

"""
##PARK DETECTION
"""
def detect_park(frame, hsv):

    mask_white = cv2.inRange(hsv, lower_white_park, upper_white_park)
    mask_green = cv2.inRange(hsv, lower_green_park, upper_green_park)
    mask_yellow = cv2.inRange(hsv, lower_yellow_park, upper_yellow_park)

    mask = mask_green + mask_yellow + mask_white

    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=2)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=1)

    img = cv2.bitwise_and(frame,frame,mask = mask)
    
    # contours detection and finding shapes
    height, width = mask.shape[:2]
    cfg.ROOF = int(height*cfg.ROOF_R)
    cfg.HORIZON = int(height*cfg.HORIZON_R)
    contours, _ = cv2.findContours(mask[0:cfg.HORIZON, 0:width], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    result = False

    for cnt in contours:
        area = cv2.contourArea(cnt)
        approx = cv2.approxPolyDP(cnt, 0.01 * cv2.arcLength(cnt, True), True)

        x,y,w,h = cv2.boundingRect(cnt)
        roi = frame[y:y+h, x:x+w]

        vr = valid_range(x,y,w,h,frame)

        if not vr:
            continue

        # calculate ratio of sides - anything not square is not worth checking
        sr = is_squarish(h, w)
        if not sr:
            continue
        if cfg.AREA_SIZE_PARK < area < cfg.MAX_AREA_SIZE: 
            if cfg.DEMO_MODE:
                cv2.rectangle(frame, (x-3,y-3), (x+w+3, y+h+3), (0,0,255), 1)
                cv2.putText(frame, "Park", (x,y), cfg.FONT, 1, (0,0,255))
            result = True
            if len(cnt) == 4:
                print("square!!") #TODO We dont need this anymore correct?
    return frame, result

"""
##CONE DETECTION
"""

def detect_cone(frame, hsv):
    mask_1 = cv2.inRange(hsv, lower_orange_cone, upper_orange_cone)
    mask_2 = cv2.inRange(hsv, lower_orange2_cone, upper_orange2_cone)
    mask = mask_1 + mask_2

    #morphology for cleaner mask
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=2)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=1)

    img = cv2.bitwise_and(frame, frame, mask = mask)

    # get contours from the mask
    height, width = mask.shape[:2]
    cfg.HORIZON = height * cfg.HORIZON_R
    cfg.ROOF = height * cfg.ROOF_R
    aboveHorizon =  - height / 8
    
    if cfg.DEMO_MODE:
        cv2.line(frame, (int(width/2),int(cfg.HORIZON + aboveHorizon )),(width, height), (0,0,255))
        cv2.line(frame, (int(width/2),int(cfg.HORIZON + aboveHorizon)),(0, height), (0,0,255))
        cv2.line(frame, (int(width/2),int(cfg.HORIZON + aboveHorizon)), (int(width/2), height), (0,0,255))


    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    largest_area = -1
    largest_cnt = None
    # find all cones larger that preset size
    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)

        if valid_range_bottom(x,y,w,h,frame) and len(cv2.approxPolyDP(cnt, 0.10 * cv2.arcLength(cnt, True), True)) == 3: # valid range bottom checks whole cone is below horizon and if shape is triangle
            area = cv2.contourArea(cnt)
            if area > largest_area and area > cfg.AREA_SIZE_CONE:
                largest_cnt = cnt
                largest_area = area

    # see if largest contour is a triangle and where it sits
    result = None
    if largest_area > 0:
        x,y,w,h = cv2.boundingRect(largest_cnt)
        # triangle detected
        if cfg.DEMO_MODE:
            cv2.rectangle(frame, (x-3,y-3), (x+w+3, y+h+3), (0,165,255), 1)
            cv2.putText(frame, "Cones", (x-3,y-5), cfg.FONT, .5, (0,165,255))

        # check if going to run into cone
        # get the middle of cone
        cone_x = x + int(w/2)
        cone_y = y + int(h/2)
        cone_center = (cone_x, cone_y)
        frame_mid = (width/2, height/2 + aboveHorizon)

        if cfg.DEMO_MODE: cv2.circle(frame, cone_center, 1, (0,165,255), 1)

        # conversion is needed for corner to corner diagnonal
        x2y = height/width
        # checks if cone center is in the path
        path_y = int (frame_mid[1]+ (abs(frame_mid[0] - cone_x)* x2y))

        if cone_center[1] < path_y:
            # checks if cone is in the path of the car
            result = 'not_in_path'
        elif cone_center[0] < frame_mid[0]:
            # if triangle mid point on left, set result to left
            result = 'left'
        else:
            result = 'right'
        # result = True
    return frame, result





