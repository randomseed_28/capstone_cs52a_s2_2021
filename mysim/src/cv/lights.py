'''
# Traffic light detection
'''

import cv2
import numpy as np

import time

from src import config as cfg
from src.cv.func import sign_direction, direction_check, valid_range, is_squarish

## FILTERS
# brightness filter
green_upper = np.array([101, 100, 100])
green_lower = np.array([78, 80, 80])
red1_lower = np.array([349, 80, 80])
red1_upper = np.array([360, 100, 100])
red2_lower = np.array([0, 80, 80])
red2_upper = np.array([10, 100, 100])
yellow_upper = np.array([50, 100, 100])
yellow_lower = np.array([40, 80, 80])


# For changing colour counter filter values in light_detect,
#  go to the function where the counter is.

# set kernel for operations
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, ksize = (cfg.TRAFFIC_KERNEL_SIZE, cfg.TRAFFIC_KERNEL_SIZE))


def light_signal(img):
    # bgr pictures into hsv space
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # For adjusting Contrast, Brightness (very slow)
    alpha = 1
    beta = 0


    # shape pointer
    shape = hsv

    # Get picture shape information
    rows,cols,dims=shape.shape

    # Separate picture channel
    h, s, v = cv2.split(shape)

    # Pixel color count
    g_counter=0
    r_counter=0
    y_counter=0

    bright_counter=0

    # COLOUR FILTER - THIS IS WHERE THE COLOURS GET FILTERED OUT
    for i in range(rows):
        for j in range(cols):
            if (78 <= h[i,j] <= 101) and (v[i,j] <= 100):
                print("green")
                g_counter += 1
            elif ((349 <= h[i,j] <= 360) or (0 <= h[i,j] <= 14)) and (v[i,j] <= 100):
                r_counter += 1
                print("red")
            elif (42 <= h[i,j] <= 53) and (v[i,j] <= 100):
                y_counter += 1
                print("amber")

    if g_counter > cfg.COUNTER_THRESHOLD_GREEN:
        return "green"
        #return g_counter
    if r_counter > cfg.COUNTER_THRESHOLD_RED:
        return "red"
        #return r_counter
    if y_counter > cfg.COUNTER_THRESHOLD_AMBER:
        return "amber"
        #return r_counter
    else :
        return False

def detect_traffic(frame, hsv):
    height, width = frame.shape[:2]
    global ROOF, HORIZON
    ROOF = int(height*cfg.ROOF_R)
    HORIZON = int(height*cfg.ROOF_R)

    mask_red1 = cv2.inRange(hsv, red1_lower, red1_upper)
    mask_red1 = cv2.morphologyEx(mask_red1, cv2.MORPH_OPEN, kernel)
    mask_red1 = cv2.morphologyEx(mask_red1, cv2.MORPH_CLOSE, kernel)

    mask_red2 = cv2.inRange(hsv, red2_lower, red2_upper)
    mask_red2 = cv2.morphologyEx(mask_red2, cv2.MORPH_OPEN, kernel)
    mask_red2 = cv2.morphologyEx(mask_red2, cv2.MORPH_CLOSE, kernel)

    mask_yellow = cv2.inRange(hsv, yellow_lower, yellow_upper)
    mask_yellow = cv2.morphologyEx(mask_yellow, cv2.MORPH_OPEN, kernel)
    mask_yellow = cv2.morphologyEx(mask_yellow, cv2.MORPH_CLOSE, kernel)
    
    mask_green = cv2.inRange(hsv, green_lower, green_upper)
    mask_green = cv2.morphologyEx(mask_green, cv2.MORPH_OPEN, kernel)
    mask_green = cv2.morphologyEx(mask_green, cv2.MORPH_CLOSE, kernel)

    mask = mask_green + mask_yellow + mask_red1 + mask_red2

    # contours detection
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    largest_area = -1
    largest_contour = None
    mask = cv2.bitwise_and(frame, frame, mask = mask)
    for cnt in contours:

        x,y,w,h = cv2.boundingRect(cnt)

        vr = valid_range(x, y, w, h, frame)
        if not vr:
            continue
        else:
            area = cv2.contourArea(cnt)
            if area > cfg.AREA_SIZE_TRAFFIC and area > largest_area:
                largest_area = area
                largest_contour = cnt
        sr = is_squarish(h, w)
        if not sr:
            continue

    result = False
    if largest_area > 0:
        print("yes it gets to here")
        x,y,w,h = cv2.boundingRect(largest_contour)
        roi_img = frame[y:y+h, x:x+w]
        result = light_signal(roi_img)
        if result and cfg.DEMO_MODE:
            cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,0), 2)
            cv2.putText(frame, result, (x+w, y+h), cfg.FONT, 1, (0,0,0), 1, cv2.LINE_AA)
    return frame, result