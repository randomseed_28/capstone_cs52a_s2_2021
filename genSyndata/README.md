# SynDataGeneration 

This code is used to generate synthetic data for object detection. This file separates object from the original image and combines it with other background images without object. A batch of data needs to be manually annotated first to generate the Pascal VOC annotation format.

## Pre-requisites 
1. OpenCV
2. Numpy

## Content structure
For the files to run smoothly, first make sure that each XML file in input_dir corresponds to a JPG file with the same name, such as:
```bash
.
└── input_dir
    ├── 1_cam_image_array_.jpg
    ├── 1_cam_image_array_.xml
    ├── 2_cam_image_array_.jpg
    ├── 2_cam_image_array_.xml
    ...
    ├── 999_cam_image_array_.jpg
    └── 999_cam_image_array_.xml
```
After running createmaskfile.py, because not each foreground image has the cut result, the directory will be changed to:
```bash
.
└── input_dir
    ├── 1_cam_image_array_.jpg
    ├── 1_cam_image_array_.xml
    ├── 2_cam_image_array_.jpg
    ├── 2_cam_image_array_.xml
    ├── 2_cam_image_array_.npy
    ...
    ├── 999_cam_image_array_.jpg
    └── 999_cam_image_array_.xml
```
After running decodeMask.py, the directory becomes:
```bash
.
└── output_dir
    ├── 0.jpg
    ├── 0.xml
    ├── 1.jpg
    ├── 1.xml
    ...
    ├── 99.jpg
    └── 99.xml
```

## Running the Script
Run `createMaskfile.py`
```
usage: python createMaskfile.py [-h] [--input_dir] [--ratio]


optional arguments:
  -h, --help         show this help message and exit
  --input_dir        input annotated directory
  --ratio            filter masks with good segment results
  --label            select a label that you want to generate. This label should be the same as the object name in the XML

```
And then, run `decodeMask.py`
```
usage: python dataset_generator.py [-h] [--input_dir] [--back_dir] [--output_dir] [--blur] [--num] [--tls] [--ctn] [--brt_value]


optional arguments:
  -h, --help         show this help message and exit
  --input_dir        input mask directory
  --back_dir         input background images directory
  --output_dir       output dataset directory
  --blur             if use Gaussian blur
  --num              number of sym data you need
  --tls              if use translation motion
  --ctn              the number where the syn_data name begin
  --brt_value        a positive or negative value to alter brightness
```
