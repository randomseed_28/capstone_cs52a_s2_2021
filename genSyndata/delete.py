import os

def del_files(path):
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith(".npy"):
                os.remove(os.path.join(root,name))
                print ("Delete File: " + os.path.join(root, name))

# test
if __name__ == "__main__":
    path = '/Users/pc/Downloads/yolov5data_2/p-blue_black-all'
    del_files(path)