import cv2
import os
import numpy as np
import xml.etree.ElementTree as ET
import argparse

def getCutRect(xml_path):
    tree=ET.parse(xml_path)
    root = tree.getroot()
    size = root.find('size')
    size = [int(size.find('width').text),int(size.find('height').text),int(size.find('depth').text)]

    # allbbox=[]
    # cls=[]
    dic={}
    for obj in root.iter('object'):
        # cls.append(obj.find('name').text)
        xmlbox = obj.find('bndbox')
        b = [int(xmlbox.find('xmin').text), int(xmlbox.find('xmax').text), int(xmlbox.find('ymin').text),
             int(xmlbox.find('ymax').text)]
        # allbbox.append(b)
        dic[obj.find('name').text] = b

    return size,dic

def dcdCut(fore,bbox):

    bbx1, bbx2, bby1, bby2 = bbox
    rect = (bbx1, bby1, bbx2 - bbx1, bby2 - bby1)

    mask = np.zeros(fore.shape[:2], np.uint8)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)

    cv2.grabCut(fore, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)

    mask2 = np.where((mask == 1) | (mask == 3), 255, 0).astype('uint8')
    b = mask2.flatten()
    count = np.count_nonzero(b)
    area=(bbox[1] - bbox[0]) * (bbox[3] - bbox[2])
    ratio = count / area
    return mask2,ratio,area

def main(args):

    xml_list = []
    for x in os.listdir(args.input_dir):
        if x.endswith('xml'):
            xml_list.append(x)
    # print(xml_list)
    jpg_list = []
    for x in os.listdir(args.input_dir):
        if x.endswith('jpg'):
            jpg_list.append(x)

    for xmlfile in xml_list:
        xmlPath = os.path.join(args.input_dir, xmlfile)
        size, dic = getCutRect(xmlPath)

        if xmlfile.replace('.xml', '.jpg') in jpg_list:
            fore_img = cv2.imread(xmlPath.replace('.xml','.jpg'))
        else:
            break

        lst=[]
        for k,v in dic.items():
            if k!='traffic_lights_A' and k!='traffic_lights_G'and k!='traffic_lights_R' and k!='traffic_lights':
                lst.append(k)
        print(lst)
        if lst:
            for i in lst:
                del dic[i]
        print(dic)
        if not dic: break

 # k:sign name  v:bbox
        mask2,ratio,area = dcdCut(fore_img,dic['traffic_lights'])
        if ratio <= args.ratio:
            print('break')
            continue

        lst2=[]
        for k,v in dic.items():
            if k!='traffic_lights':
                lst2.append(k)
        print(lst2)

        dictionary={'img':mask2,
                    'name1':'traffic_lights',
                    'name2':lst2[0],
                    'bbox1': dic['traffic_lights'],
                    'bbox2': dic[lst2[0]],
                    'size': size}

        with open(xmlPath.replace('.xml','.npy'), 'wb') as f:
            np.save(f, dictionary)
        print(xmlPath.replace('.xml','.npy'))

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True,
                        help="input annotated directory")
    parser.add_argument("--ratio", default=0.5, type=float,
                        help="filter masks with good segment results")
    # parser.add_argument("--label", type=str,
    #                     help="choose a label you want to make mask")

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    main(args)