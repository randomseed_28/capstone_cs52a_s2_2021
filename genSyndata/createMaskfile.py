import cv2
import os
import numpy as np
import xml.etree.ElementTree as ET
import argparse


def getCutRect(xml_path):
    tree = ET.parse(xml_path)
    root = tree.getroot()
    size = root.find('size')
    size = [int(size.find('width').text), int(size.find('height').text), int(size.find('depth').text)]

    # allbbox=[]
    # cls=[]
    dic = {}
    for obj in root.iter('object'):
        # cls.append(obj.find('name').text)
        xmlbox = obj.find('bndbox')
        b = [int(xmlbox.find('xmin').text), int(xmlbox.find('xmax').text), int(xmlbox.find('ymin').text),
             int(xmlbox.find('ymax').text)]
        # allbbox.append(b)
        dic[obj.find('name').text] = b

    return size, dic


def dcdCut(fore, bbox):
    # Initialize the rectangle region
    # The rectangle must contain the foreground completely
    bbx1, bbx2, bby1, bby2 = bbox
    rect = (bbx1, bby1, bbx2 - bbx1, bby2 - bby1)

    # Create mask background and foreground images
    mask = np.zeros(fore.shape[:2], np.uint8)  # Create a mask of the same size
    bgdModel = np.zeros((1, 65), np.float64)  # Create background image
    fgdModel = np.zeros((1, 65), np.float64)  # Create foreground image

    # GrubCut algorithm, 5 iterations
    cv2.grabCut(fore, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)  # 迭代5次

    mask2 = np.where((mask == 1) | (mask == 3), 255, 0).astype('uint8')

    # Calculate cut ratio
    b = mask2.flatten()
    count = np.count_nonzero(b)
    area = (bbox[1] - bbox[0]) * (bbox[3] - bbox[2])
    ratio = count / area
    return mask2, ratio, area


def filter(masklst, ratio, label):
    # For each mask, only the object with the largest area is selected to avoid overlapping Because of random change of position
    filter_lst = []
    # filter good mask firstly
    for i in masklst:  # i:[mask2,ratio,area,bbox,sign name]
        if i[1] >= ratio:
            filter_lst.append(i)

    filter_lst2 = []
    if not filter_lst:
        return filter_lst
    else:
        if label:
            for i in filter_lst:
                if i[4] == label:
                    filter_lst2.append(i)
            if not filter_lst2: return filter_lst2

            sorted(filter_lst2, key=lambda x: x[2], reverse=True)
            return filter_lst2[0]

        else:
            sorted(filter_lst,key=lambda x:x[2],reverse=True)
            return filter_lst[0]


def main(args):
    xml_list = []
    for x in os.listdir(args.input_dir):
        if x.endswith('xml'):
            xml_list.append(x)
    # print(xml_list)
    jpg_list = []
    for x in os.listdir(args.input_dir):
        if x.endswith('jpg'):
            jpg_list.append(x)

    for xmlfile in xml_list:
        xmlPath = os.path.join(args.input_dir, xmlfile)
        size, dic = getCutRect(xmlPath)
        # dic.pop('traffic_lights_G')
        # dic.pop('traffic_lights_A')
        # dic.pop('traffic_lights_R') # Does not apply to one object has two bounding box
        if xmlfile.replace('.xml', '.jpg') in jpg_list:
            fore_img = cv2.imread(xmlPath.replace('.xml', '.jpg'))
        else:
            break

        masklst = []
        for k, v in dic.items():  # k:sign name  v:bbox
            mask2, ratio, area = dcdCut(fore_img, v)
            masklst.append([mask2, ratio, area, v, k])

        filter_mask = filter(masklst, args.ratio, args.label)
        if filter_mask:
            dictionary = {'img': filter_mask[0], 'bbox': filter_mask[3], 'name': filter_mask[4], 'size': size}

            with open(xmlPath.replace('.xml', '.npy'), 'wb') as f:
                np.save(f, dictionary)
            print(xmlPath.replace('.xml', '.npy'))


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True,
                        help="input annotated directory")
    parser.add_argument("--ratio", default=0.5, type=float,
                        help="filter masks with good segment results")
    parser.add_argument("--label", type=str,
                        help="choose a label you want to make mask")

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    main(args)