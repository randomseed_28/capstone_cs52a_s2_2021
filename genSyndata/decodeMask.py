import cv2
import numpy as np
import argparse
import xml.dom.minidom
from xml.etree.ElementTree import Element, SubElement, tostring
import os
import random

def sizeControl():
    pass

def change_brightness(img, value):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)
    v = cv2.add(v,value)
    v[v > 255] = 255
    v[v < 0] = 0
    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def motion(width, height, bbox):
    xmin, xmax, ymin, ymax = bbox
    x = random.randint(0, int(width) - (xmax - xmin))
    y = random.randint(0, int(height) - (ymax - ymin))
    new_bbox = [x, x + (xmax - xmin), y, y + (ymax - ymin)]
    return new_bbox

def blur(mask):
    # print('use blur')
    se = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    cv2.dilate(mask, se, mask)
    mask = cv2.GaussianBlur(mask, (5, 5), 0)
    return mask

def gen_syn_img(args, fg_file, bg_file, idx):
    fg_file_path = os.path.join(args.input_dir, fg_file)
    bg_file_path = os.path.join(args.back_dir,bg_file)

    dic = np.load(fg_file_path,allow_pickle=True).item()

    size = dic['size']
    width = dic['size'][0] # The size of the generated image matches the size of the foreground image
    height = dic['size'][1]
    object_type = dic['name']
    bbox = dic['bbox']
    mask = dic['img']

    if args.tls:
        new_bbox = motion(width,height,bbox)
    else: new_bbox = bbox

    fore = cv2.imread(fg_file_path.replace('.npy', '.jpg'))
    back = cv2.imread(bg_file_path)
    back = cv2.resize(back,(width,height))

    if args.blur:
        mask = blur(mask)

    mask = mask / 255.0  # (240, 240)
    a = mask[..., None]  # (240, 240, 1)

    # replace partial background image + com=a*fg+(1-a)*bg
    b = a[bbox[2]:bbox[3], bbox[0]:bbox[1]]
    back[new_bbox[2]:new_bbox[3], new_bbox[0]:new_bbox[1]] = (1 - b) * (back[new_bbox[2]:new_bbox[3], new_bbox[0]:new_bbox[1]].astype(np.float32)) \
                                                             + b * (change_brightness(fore[bbox[2]:bbox[3], bbox[0]:bbox[1]], args.brt_value).astype(np.float32))

    cv2.imwrite(args.output_dir + '/{}.jpg'.format(idx), back)

    return size, object_type, new_bbox

def createXMLfile(output_dir,size,new_bbox,object_type,idx):
    xmin, xmax, ymin, ymax = new_bbox
    width,height,depth=size
    folder = output_dir.split('/')[-1]

    top = Element('annotation')

    folder_entry = SubElement(top, 'folder')
    folder_entry.text = str(folder)
    file_name_entry = SubElement(top, 'filename')
    file_name_entry.text = str(idx)+'.jpg'
    path_entry = SubElement(top, 'path')
    path_entry.text = os.path.join(output_dir,str(idx)+'.jpg')

    source_root = SubElement(top,'source')
    database_entry = SubElement(source_root, 'database')
    database_entry.text = 'Unknown'

    size_root = SubElement(top, 'size')
    width_entry = SubElement(size_root, 'width')
    width_entry.text = str(width)
    height_entry = SubElement(size_root, 'height')
    height_entry.text = str(height)
    depth_entry = SubElement(size_root, 'depth')
    depth_entry.text = str(depth)

    segmented_entry = SubElement(top, 'segmented')
    segmented_entry.text = '0'

    object_root = SubElement(top, 'object')
    object_type_entry = SubElement(object_root, 'name')
    object_type_entry.text = str(object_type)
    pose_entry = SubElement(object_root, 'pose')
    pose_entry.text = 'Unspecified'
    truncated_entry = SubElement(object_root, 'truncated')
    truncated_entry.text = '0'
    difficult_entry = SubElement(object_root, 'difficult')
    difficult_entry.text = '0'

    object_bndbox_entry = SubElement(object_root, 'bndbox')
    x_min_entry = SubElement(object_bndbox_entry, 'xmin')
    x_min_entry.text = '%d' % (xmin)
    x_max_entry = SubElement(object_bndbox_entry, 'xmax')
    x_max_entry.text = '%d' % (xmax)
    y_min_entry = SubElement(object_bndbox_entry, 'ymin')
    y_min_entry.text = '%d' % (ymin)
    y_max_entry = SubElement(object_bndbox_entry, 'ymax')
    y_max_entry.text = '%d' % (ymax)

    xmlstr = xml.dom.minidom.parseString(tostring(top)).toprettyxml(indent="    ")
    with open(os.path.join(output_dir,str(idx)+'.xml'), "w") as f:
        f.write(xmlstr)

def main(args):
    background_files = []
    for x in os.listdir(args.back_dir):
        if x.endswith('jpg'):
            background_files.append(x)
    mask_file = []
    for x in os.listdir(args.input_dir):
        if x.endswith('npy'):
            mask_file.append(x)

    for idx in range(args.num):
        bg_file = random.choice(background_files)
        fg_file = random.choice(mask_file)

        print(bg_file, fg_file)

        size, object_type, new_bbox = gen_syn_img(args, fg_file, bg_file, idx+args.ctn)
        createXMLfile(args.output_dir, size, new_bbox, object_type, idx+args.ctn)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True,
                        help="input annotated directory")
    parser.add_argument("--back_dir", type=str, required=True,
                        help="input background images directory")
    parser.add_argument("--output_dir", type=str, required=True,
                        help="output dataset directory")
    parser.add_argument("--blur", action='store_false',
                        help="if use Gaussian blur, if not pass, default to True")
    parser.add_argument("--num", type=int, required=True,
                        help="number of syn data you need")
    parser.add_argument("--tls", action='store_false',
                        help="if use translation motion, if not pass, default to True")
    parser.add_argument("--ctn", default=0, type=int,
                        help="the number where the syn_data name begin")
    parser.add_argument("--brt_value", type=int, required=True,
                        help="a positive or negative value to alter brightness")

    return parser.parse_args()

    # parser.add_argument("--size", default=416, type=int,
    #                     help="the size of synthesis data")

if __name__ == '__main__':
    args = get_args()
    main(args)
