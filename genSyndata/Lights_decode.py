import cv2
import numpy as np
import argparse
import xml.dom.minidom
from xml.etree.ElementTree import Element, SubElement, tostring
import os
import random

def sizeControl():
    pass

def change_brightness(img, value):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)
    v = cv2.add(v,value)
    v[v > 255] = 255
    v[v < 0] = 0
    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def motion(width, height, bbox1, bbox2):
    xmin1, xmax1, ymin1, ymax1 = bbox1
    xmin2, xmax2, ymin2, ymax2 = bbox2
    x = random.randint(0, int(width) - (xmax1 - xmin1))
    y = random.randint(0, int(height) - (ymax1 - ymin1))
    new_bbox1 = [x, x + (xmax1 - xmin1), y, y + (ymax1 - ymin1)]
    new_bbox2 = [x+xmin2-xmin1, x + (xmax1 - xmin1)+ xmax2-xmax1, y+ymin2-ymin1, y + (ymax1 - ymin1)+ymax2-ymax1]
    return new_bbox1, new_bbox2

def blur(mask):
    # print('use blur')
    se = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    cv2.dilate(mask, se, mask)
    mask = cv2.GaussianBlur(mask, (5, 5), 0)
    return mask

def gen_syn_img(args, fg_file, bg_file, idx):
    fg_file_path = os.path.join(args.input_dir, fg_file)
    bg_file_path = os.path.join(args.back_dir,bg_file)

    dic = np.load(fg_file_path,allow_pickle=True).item()

    size = dic['size']
    width = dic['size'][0] # The size of the generated image matches the size of the foreground image
    height = dic['size'][1]

    object_type1 = dic['name1']
    object_type2 = dic['name2']
    bbox1 = dic['bbox1']
    bbox2 = dic['bbox2']
    mask = dic['img']

    if args.tls:
        new_bbox1,new_bbox2 = motion(width,height,bbox1,bbox2)
    else: new_bbox1,new_bbox2 = bbox1,bbox2

    fore = cv2.imread(fg_file_path.replace('.npy', '.jpg'))
    back = cv2.imread(bg_file_path)
    back = cv2.resize(back,(width,height))

    if args.blur:
        mask = blur(mask)

    mask = mask / 255.0  # (240, 240)
    a = mask[..., None]  # (240, 240, 1)

    b = a[bbox1[2]:bbox1[3], bbox1[0]:bbox1[1]]
    back[new_bbox1[2]:new_bbox1[3], new_bbox1[0]:new_bbox1[1]] = (1 - b) * (back[new_bbox1[2]:new_bbox1[3], new_bbox1[0]:new_bbox1[1]].astype(np.float32)) \
                                                             + b * (change_brightness(fore[bbox1[2]:bbox1[3], bbox1[0]:bbox1[1]], args.brt_value).astype(np.float32))

    cv2.imwrite(args.output_dir + '/{}.jpg'.format(idx), back)

    return size, object_type1,object_type2, new_bbox1,new_bbox2

def createXMLfile(output_dir,size,new_bbox1,new_bbox2,object_type1,object_type2,idx):
    xmin1, xmax1, ymin1, ymax1 = new_bbox1
    xmin2, xmax2, ymin2, ymax2 =new_bbox2
    width,height,depth=size
    folder = output_dir.split('/')[-1]

    top = Element('annotation')

    folder_entry = SubElement(top, 'folder')
    folder_entry.text = str(folder)
    file_name_entry = SubElement(top, 'filename')
    file_name_entry.text = str(idx)+'.jpg'
    path_entry = SubElement(top, 'path')
    path_entry.text = os.path.join(output_dir,str(idx)+'.jpg')

    source_root = SubElement(top,'source')
    database_entry = SubElement(source_root, 'database')
    database_entry.text = 'Unknown'

    size_root = SubElement(top, 'size')
    width_entry = SubElement(size_root, 'width')
    width_entry.text = str(width)
    height_entry = SubElement(size_root, 'height')
    height_entry.text = str(height)
    depth_entry = SubElement(size_root, 'depth')
    depth_entry.text = str(depth)

    segmented_entry = SubElement(top, 'segmented')
    segmented_entry.text = '0'

    object_root1 = SubElement(top, 'object')
    object_type_entry1 = SubElement(object_root1, 'name')
    object_type_entry1.text = str(object_type1)
    pose_entry1 = SubElement(object_root1, 'pose')
    pose_entry1.text = 'Unspecified'
    truncated_entry1 = SubElement(object_root1, 'truncated')
    truncated_entry1.text = '0'
    difficult_entry1 = SubElement(object_root1, 'difficult')
    difficult_entry1.text = '0'

    object_bndbox_entry1 = SubElement(object_root1, 'bndbox')
    x_min_entry1 = SubElement(object_bndbox_entry1, 'xmin')
    x_min_entry1.text = '%d' % (xmin1)
    x_max_entry1 = SubElement(object_bndbox_entry1, 'xmax')
    x_max_entry1.text = '%d' % (xmax1)
    y_min_entry1 = SubElement(object_bndbox_entry1, 'ymin')
    y_min_entry1.text = '%d' % (ymin1)
    y_max_entry1 = SubElement(object_bndbox_entry1, 'ymax')
    y_max_entry1.text = '%d' % (ymax1)

    object_root2 = SubElement(top, 'object')
    object_type_entry2 = SubElement(object_root2, 'name')
    object_type_entry2.text = str(object_type2)
    pose_entry2 = SubElement(object_root2, 'pose')
    pose_entry2.text = 'Unspecified'
    truncated_entry2 = SubElement(object_root2, 'truncated')
    truncated_entry2.text = '0'
    difficult_entry2 = SubElement(object_root2, 'difficult')
    difficult_entry2.text = '0'

    object_bndbox_entry2 = SubElement(object_root2, 'bndbox')
    x_min_entry2 = SubElement(object_bndbox_entry2, 'xmin')
    x_min_entry2.text = '%d' % (xmin2)
    x_max_entry2 = SubElement(object_bndbox_entry2, 'xmax')
    x_max_entry2.text = '%d' % (xmax2)
    y_min_entry2 = SubElement(object_bndbox_entry2, 'ymin')
    y_min_entry2.text = '%d' % (ymin2)
    y_max_entry2 = SubElement(object_bndbox_entry2, 'ymax')
    y_max_entry2.text = '%d' % (ymax2)

    xmlstr = xml.dom.minidom.parseString(tostring(top)).toprettyxml(indent="    ")
    with open(os.path.join(output_dir,str(idx)+'.xml'), "w") as f:
        f.write(xmlstr)

def main(args):
    background_files = []
    for x in os.listdir(args.back_dir):
        if x.endswith('jpg'):
            background_files.append(x)
    mask_file = []
    for x in os.listdir(args.input_dir):
        if x.endswith('npy'):
            mask_file.append(x)

    for idx in range(args.num):
        bg_file = random.choice(background_files)
        fg_file = random.choice(mask_file)

        print(bg_file, fg_file)

        size, object_type1,object_type2, new_bbox1,new_bbox2 = gen_syn_img(args, fg_file, bg_file, idx+args.ctn)
        createXMLfile(args.output_dir, size, new_bbox1, new_bbox2,object_type1, object_type2, idx+args.ctn)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True,
                        help="input annotated directory")
    parser.add_argument("--back_dir", type=str, required=True,
                        help="input background images directory")
    parser.add_argument("--output_dir", type=str, required=True,
                        help="output dataset directory")
    parser.add_argument("--blur", action='store_false',
                        help="if use Gaussian blur, if not pass, default to True")
    parser.add_argument("--num", type=int, required=True,
                        help="number of syn data you need")
    parser.add_argument("--tls", action='store_false',
                        help="if use translation motion, if not pass, default to True")
    parser.add_argument("--ctn", default=0, type=int,
                        help="the number where the syn_data name begin")
    parser.add_argument("--brt_value", type=int,
                        help="a positive or negative value to alter brightness")

    return parser.parse_args()

    # parser.add_argument("--size", default=416, type=int,
    #                     help="the size of synthesis data")

if __name__ == '__main__':
    args = get_args()
    main(args)
