# README #
This is the USYD Capstone Project Repository of CS52A - Autonomous cars using neural networks and computer vision. 

# File construction #
The signDetection folder is to detect traffic signs based on YOLOv5 algorithm.

The genSyndata folder is used to make artificial images and annotations.

The mysim folder is the car we create by```donkey createcar --path ~/mycar```. We added self-driving model, sign detection model and its related code, and action response folder.

The sandbox_for_NN_linux folder is the simulator we built for linux.

The donkeycar_sandbox_windows folder is the simulator we built for windows.


# How to use #

```python
git clone https://Doooong@bitbucket.org/randomseed_28/capstone_cs52a_s2_2021.git
```

# Other resources #
* Raw Data
```python
  https://unisydneyedu-my.sharepoint.com/:f:/g/personal/hzhu2202_uni_sydney_edu_au/El-eTSYO761OnuZaZ2eF96MBTlhrurmTsRw-QiQBVUI6DQ?e=MIfDDp
```
* Dataset (VOC format)
```python
git clone https://Doooong@bitbucket.org/zpqzpq912/cs52a_s2_2021_dataset.git
```
* Deep Learning pipeline
```python
https://bitbucket.org/zpqzpq912/mlpipeline/src/master/
```
* DonkeyCar Documents: https://docs.donkeycar.com/
* DonkeyCar Github: https://github.com/autorope/donkeycar

