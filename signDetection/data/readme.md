# Data preprocessing

This code is used for data preprocessing to process labeled data into the format required by the detection network.

# Execution sequence

1. get_all_xml_jpg.py  --  Name the data uniformly
2. xmlCheck.py  --  Check whether there are some problems with the content of the XML file
3. create_voc.py  --  Generate txt files required for model training
4. div_train_val.py  --  Divide training set and validation set

# Pre-requisites 

1. lxml

`pip install lxml`

# Running the Script

Run `get_all_xml_jpg.py`

```
usage: python get_all_xml_jpg.py [-h] [--root_path ROOT_PATH]


optional arguments:
  -h, --help                show this help message and exit
  --root_path ROOT_PATH     all xml and image file path
```

Run `xmlCheck.py`

```
usage: python xmlCheck.py [-h] [--xml_path XML_PATH] [--image_path IMAGE_PATH]


optional arguments:
  -h, --help                show this help message and exit
  --xml_path XML_PATH       xml file path
  --image_path IMAGE_PATH   image file path
```

Run `create_voc.py`

```
usage: python create_voc.py [-h] [--root_path ROOT_PATH]


optional arguments:
  -h, --help               show this help message and exit
  --root_path ROOT_PATH    xml and image root file path
```

Run `div_train_val.py`

```
usage: python div_train_val.py [-h] [--root_path ROOT_PATH]


optional arguments:
  -h, --help              show this help message and exit
  --root_path ROOT_PATH   xml and image root file path
```

# Change profile

In VOC.yaml:

![path](D:\2021S2\capstone\final\path.png)

You need to change the path of the train and val to the path where you store the training data and validation data(create from div_train_val.py).

