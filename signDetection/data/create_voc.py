import xml.etree.ElementTree as ET
from lxml import etree
import pickle
import os
from os import listdir, getcwd
from os.path import join
import re
import shutil
import argparse
# -*- coding: UTF-8 -*-


'''
TXT file content record format is: label x_center y_center width height
'''

#label list
classes = ['black_left_round', 'black_right_round', 'black_white_left', 'black_white_right',
              'blue_left_arrow', 'blue_right_arrow', 'blue_left_round', 'orange_cone', 'park_green',
              'park_white', 'park_yellow', 'blue_right_round', 'stop', 'traffic_lights_A', "traffic_lights_G",
              'traffic_lights_R', 'speed_5', 'speed_10', 'speed_25', 'speed_40', 'speed_50']



def convert(size, box):
    dw = 1./(size[0])
    dh = 1./(size[1])
    x = (box[0] + box[1])/2.0 - 1
    y = (box[2] + box[3])/2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def convert_annotation(image_id, root_xml_path, root_jpg_path, root_labels_path):
    xml_name = root_xml_path + '/' + image_id + ".xml"
    in_file = open(xml_name)
    jpg_name = root_jpg_path + '/' + image_id + ".jpg"
    out_file = open(root_labels_path + '/' + image_id + ".txt", 'w')
    tree=ET.parse(in_file)
    root = tree.getroot()
    size = root.find('size')
    w = int(size.find('width').text)
    h = int(size.find('height').text)
    #print(w, h)
    for obj in root.iter('object'):
        #difficult = obj.find('difficult').text
        difficult = 0
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
            continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        #print(xmlbox)
        b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))

        bb = convert((w,h), b)
        x1, y1, w1, h1 = bb
        if x1 < 0 or y1 < 0 or w1 <= 0 or h1 <= 0:
            print(xml_name)
            if not os.path.exists(xml_name.replace('Annotations', 'xml_waste')):
                os.makedirs(xml_name.replace('Annotations', 'xml_waste'))
                os.makedirs(jpg_name.replace('JPEGImages', 'jpg_waste'))
            shutil.move(xml_name, xml_name.replace('Annotations', 'xml_waste'))
            shutil.move(jpg_name, jpg_name.replace('JPEGImages', 'jpg_waste'))
        out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--root_path', type=str, default=r'D:\2021S2\capstone\data2022\testData', help='xml and image root file path')
    opt = parser.parse_args()

    root_path = opt.root_path
    root_xml_path = os.path.join(root_path, "Annotations")
    root_jpg_path = os.path.join(root_path, "JPEGImages")

    root_labels_path = os.path.join(root_path, "labels")

    if not os.path.exists(root_labels_path):
        os.makedirs(root_labels_path)
    image_ids = os.listdir(root_jpg_path)

    i = 0
    for image_name in image_ids:
        image_id = os.path.splitext(image_name)[0]
        convert_annotation(image_id, root_xml_path, root_jpg_path, root_labels_path)
        i += 1
