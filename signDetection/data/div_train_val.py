import os
import shutil
import random
import argparse


def div_train_val(root_path):
    jpg_path = root_path + '/JPEGImages'
    out_val_jpg_path = root_path + '/VOC/images/val'
    out_train_jpg_path = root_path + '/VOC/images/train'
    label_path = root_path + '/labels'
    out_val_label_path = root_path + '/VOC/labels/val'
    out_train_label_path = root_path + '/VOC/labels/train'
    os.makedirs(out_val_jpg_path)
    os.makedirs(out_train_jpg_path)
    os.makedirs(out_val_label_path)
    os.makedirs(out_train_label_path)
    jpg_name_list = os.listdir(jpg_path)
    random.shuffle(jpg_name_list)
    i = 0
    for jpg_name in jpg_name_list:
        in_jpg_path = os.path.join(jpg_path, jpg_name)
        out_val_jpg_path_out = os.path.join(out_val_jpg_path, jpg_name)
        out_train_jpg_path_out = os.path.join(out_train_jpg_path, jpg_name)
        label_name = jpg_name.replace('.jpg', '.txt')
        in_label_path = os.path.join(label_path, label_name)
        out_val_label_path_out = os.path.join(out_val_label_path, label_name)
        out_train_label_path_out = os.path.join(out_train_label_path, label_name)
        if i%9 == 0:
            shutil.move(in_jpg_path, out_val_jpg_path_out)
            shutil.move(in_label_path, out_val_label_path_out)
        else:
            shutil.move(in_jpg_path, out_train_jpg_path_out)
            shutil.move(in_label_path, out_train_label_path_out)
        i += 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--root_path', type=str, default=r'D:\2021S2\capstone\data2022\testData', help='xml and image root file path')
    opt = parser.parse_args()
    div_train_val(opt.root_path)