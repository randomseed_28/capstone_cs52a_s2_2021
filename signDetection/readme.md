# How to run

example:

```
python train.py --img-size=416 --cfg=./models/yolov5s_se.yaml --data=./data/voc.yaml --batch-size=16 --epochs=100
```

